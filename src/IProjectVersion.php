<?php

declare(strict_types=1);

namespace ElektroPotkan\ProjectVersion;


/**
 * Project version metadata interface
 */
interface IProjectVersion {
	/**
	 * Returns version of the top-level project itself
	 */
	function getVersion(): string;
} // interface IProjectVersion
