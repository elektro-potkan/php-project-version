<?php

declare(strict_types=1);

namespace ElektroPotkan\ProjectVersion;

use Nette;


/**
 * Stub returning version string given to constructor
 * @property-read string $version
 */
class ProjectVersionStatic implements IProjectVersion {
	use Nette\SmartObject;
	
	
	/** @var string */
	private $version;
	
	
	/**
	 * Constructor
	 */
	public function __construct(string $version){
		$this->version = $version;
	} // constructor
	
	/**
	 * Returns version of the top-level project itself
	 */
	public function getVersion(): string {
		return $this->version;
	} // getVersion
} // class ProjectVersionStatic
