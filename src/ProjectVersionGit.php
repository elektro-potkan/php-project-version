<?php

declare(strict_types=1);

namespace ElektroPotkan\ProjectVersion;

use Nette;


/**
 * Project version from GIT
 * @property-read string $version
 */
class ProjectVersionGit implements IProjectVersion {
	use Nette\SmartObject;
	
	
	/** @var string */
	private $dir;
	
	/** @var string */
	private $version = null;
	
	
	/**
	 * Constructor
	 * @param string $dir - top-level project base directory (to run GIT in)
	 */
	public function __construct(?string $dir = null){
		if($dir === null){
			$dir = dirname(dirname(dirname(dirname(__DIR__))));
		};
		$this->dir = $dir;
	} // constructor
	
	/**
	 * Returns version of the top-level project itself
	 */
	public function getVersion(): string {
		if($this->version === null){
			$this->version = exec('cd '.escapeshellarg($this->dir).' && git describe --always');
		};
		
		return $this->version;
	} // getVersion
} // class ProjectVersionGit
