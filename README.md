Project Version Info
====================

Retrieves version of project itself - using git or other methods.


Usage
-----
This package mainly serves as common interface to get version of the project itself.
The usual way is to instantiate the GIT variant during development and later use the static stub on a production server.

```php
// development
$projectVersion = new ElektroPotkan\ProjectVersion\ProjectVersionGit;

// production
$projectVersion = new ElektroPotkan\ProjectVersion\ProjectVersionStatic('v4.7.28');

// retrieving the version string
$projectVersion->getVersion();
```

You could also implement the interface `IProjectVersion` to provide Your own version source.


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.

See file [LICENSE](LICENSE.md).
