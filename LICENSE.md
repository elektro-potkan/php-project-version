License
=======
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.


BSD Zero Clause License
-----------------------
See file [LICENSE_BSD-0](LICENSE_BSD-0).


GNU General Public License (GPL)
--------------------------------
You may use this program under the GNU General Public License version 3 or (at your option) any later version (GNU GPLv3+).

See file [LICENSE_GPL-v3](LICENSE_GPL-v3).

> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
> 
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.
